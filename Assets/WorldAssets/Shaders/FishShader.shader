// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/FishShader"
{
	Properties
	{
		_Intensity("Intensity", Range( 0 , 0.1)) = 0.01
		_Speed("Speed", Range( 0 , 100)) = 5
		_MainTex("MainTex", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
			float2 texcoord_0;
		};

		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _Speed;
		uniform float _Intensity;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			o.texcoord_0.xy = v.texcoord1.xy * float2( 1,1 ) + float2( 0,0 );
			float temp_output_24_0 = ( 1.0 - o.texcoord_0.y );
			float4 appendResult19 = (float4(( temp_output_24_0 * cos( ( _Time.y * _Speed ) ) ) , (float)0 , (float)0 , 0.0));
			v.vertex.xyz += ( ( temp_output_24_0 * appendResult19 ) * _Intensity ).xyz;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			o.Albedo = tex2D( _MainTex, uv_MainTex ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13701
1952;30;1869;1013;1819.871;317.2916;1.154428;True;True
Node;AmplifyShaderEditor.TimeNode;9;-1333.882,749.3375;Float;False;0;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;35;-1263.508,913.3835;Float;False;Property;_Speed;Speed;1;0;5;0;100;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;33;-1099.561,775.1852;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;11;-1361.054,499.7453;Float;False;1;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.CosOpNode;10;-942.5551,736.908;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;24;-1060.047,537.8231;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-793.4979,599.9503;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.IntNode;26;-794.299,436.269;Float;False;Constant;_0;0;0;0;0;0;1;INT
Node;AmplifyShaderEditor.DynamicAppendNode;19;-613.3841,550.8801;Float;False;FLOAT4;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;3;-455.5073,436.8723;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT4;0;False;1;FLOAT4
Node;AmplifyShaderEditor.RangedFloatNode;4;-602.8427,339.9536;Float;False;Property;_Intensity;Intensity;0;0;0.01;0;0.1;0;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;36;-623.5734,-134.3014;Float;True;Property;_MainTex;MainTex;2;0;Assets/WorldAssets/Textures/T_RC_ColorAtlas.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-213.8269,383.559;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;21.52346,51.65632;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Custom/FishShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;33;0;9;2
WireConnection;33;1;35;0
WireConnection;10;0;33;0
WireConnection;24;0;11;2
WireConnection;13;0;24;0
WireConnection;13;1;10;0
WireConnection;19;0;13;0
WireConnection;19;1;26;0
WireConnection;19;2;26;0
WireConnection;3;0;24;0
WireConnection;3;1;19;0
WireConnection;17;0;3;0
WireConnection;17;1;4;0
WireConnection;0;0;36;0
WireConnection;0;11;17;0
ASEEND*/
//CHKSM=2B18C49A40882EEEDEF93F338A227CEB8AD9ECB8