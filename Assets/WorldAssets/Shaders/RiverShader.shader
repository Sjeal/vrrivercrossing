// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "RiverShader"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}
		_Roughness("Roughness", Range( 0 , 1)) = 0
		_Metal("Metal", Range( 0 , 1)) = 0
		_AOContribution("AOContribution", Range( 0 , 1)) = 1
		_ColourPower("ColourPower", Range( 0 , 1)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Off
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
		};

		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _ColourPower;
		uniform float _Metal;
		uniform float _Roughness;
		uniform float _AOContribution;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float4 temp_cast_0 = (1.0).xxxx;
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 lerpResult17 = lerp( temp_cast_0 , tex2D( _MainTex, uv_MainTex ) , _ColourPower);
			o.Albedo = lerpResult17.rgb;
			o.Metallic = _Metal;
			o.Smoothness = ( 1.0 - _Roughness );
			float4 temp_cast_2 = (_AOContribution).xxxx;
			o.Occlusion = pow( i.vertexColor , temp_cast_2 ).r;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13701
1952;30;1869;1013;709.1791;501.6606;1.139297;True;True
Node;AmplifyShaderEditor.RangedFloatNode;19;-65.47627,-155.3143;Float;False;Constant;_Float0;Float 0;5;0;1;1;1;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;16;34.78119,-75.56394;Float;False;Property;_ColourPower;ColourPower;4;0;1;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;3;-425,-193.5;Float;True;Property;_MainTex;MainTex;0;0;Assets/WorldAssets/Textures/T_RC_ColorAtlas.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;11;-398.1015,250.2561;Float;False;Property;_AOContribution;AOContribution;3;0;1;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.VertexColorNode;1;-349.3661,59.82125;Float;False;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;5;-141.3423,470.8154;Float;False;Property;_Roughness;Roughness;1;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;6;140.9363,476.5118;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.LerpOp;17;100.8611,-287.4727;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0.0;False;2;FLOAT;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.RangedFloatNode;7;-132.3423,394.8153;Float;False;Property;_Metal;Metal;2;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.PowerNode;12;-55.48478,131.2524;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0;False;1;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;543.0016,23.39995;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;RiverShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexScale;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;6;0;5;0
WireConnection;17;0;19;0
WireConnection;17;1;3;0
WireConnection;17;2;16;0
WireConnection;12;0;1;0
WireConnection;12;1;11;0
WireConnection;0;0;17;0
WireConnection;0;3;7;0
WireConnection;0;4;6;0
WireConnection;0;5;12;0
ASEEND*/
//CHKSM=EA4D7F80285E5E541E583687219CA78A09A0CC24