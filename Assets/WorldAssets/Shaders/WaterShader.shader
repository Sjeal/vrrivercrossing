// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "WaterShader"
{
	Properties
	{
		_BaseColour("BaseColour", Color) = (0,0,0,0)
		_FoamColour("FoamColour", Color) = (0,0,0,0)
		_Opacity("Opacity", Range( 0 , 1)) = 0
		_Spec("Spec", Range( 0 , 1)) = 0
		_Smoothness("Smoothness", Range( 0 , 1)) = 0
		_WaveSpeedMultiply("WaveSpeedMultiply", Range( 0 , 1)) = 0
		_WaveGuide("WaveGuide", 2D) = "white" {}
		_WavePower("WavePower", Range( 0 , 1)) = 0
		_DepthFade("DepthFade", Range( 0 , 3)) = 0
		_FoamTexture("FoamTexture", 2D) = "white" {}
		_FoamUVTile("FoamUVTile", Range( 1 , 20)) = 1
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float3 worldPos;
			float2 texcoord_0;
			float4 screenPos;
			float2 texcoord_1;
		};

		uniform sampler2D _FoamTexture;
		uniform float _FoamUVTile;
		uniform float4 _FoamColour;
		uniform float4 _BaseColour;
		uniform sampler2D _CameraDepthTexture;
		uniform float _DepthFade;
		uniform float _Spec;
		uniform float _Smoothness;
		uniform float _Opacity;
		uniform sampler2D _WaveGuide;
		uniform float _WaveSpeedMultiply;
		uniform float _WavePower;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float2 temp_cast_0 = (_FoamUVTile).xx;
			o.texcoord_0.xy = v.texcoord.xy * temp_cast_0 + float2( 0,0 );
			float WaveSpeed26 = ( _Time.y * _WaveSpeedMultiply );
			float2 temp_cast_1 = (( WaveSpeed26 + 0 )).xx;
			o.texcoord_1.xy = v.texcoord.xy * float2( 1,1 ) + temp_cast_1;
			float3 ase_vertexNormal = v.normal.xyz;
			v.vertex.xyz += ( ( tex2Dlod( _WaveGuide, float4( o.texcoord_1, 0, 1.0) ).r - 0.5 ) * ( ase_vertexNormal * _WavePower ) );
		}

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 normalizeResult14 = normalize( ( cross( ddx( ase_worldPos ) , ddy( ase_worldPos ) ) + float3( 1E-09,0,0 ) ) );
			o.Normal = normalizeResult14;
			float2 panner75 = ( i.texcoord_0 + 1.0 * _Time.y * float2( -0.1,0.01 ));
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float screenDepth32 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD(ase_screenPos))));
			float distanceDepth32 = abs( ( screenDepth32 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( _DepthFade ) );
			float clampResult45 = clamp( distanceDepth32 , 0.0 , 1.0 );
			float4 lerpResult35 = lerp( ( tex2D( _FoamTexture, panner75 ).r * _FoamColour ) , _BaseColour , clampResult45);
			o.Albedo = lerpResult35.rgb;
			o.Emission = ( lerpResult35 * 0.6 ).rgb;
			float3 temp_cast_2 = (_Spec).xxx;
			o.Specular = temp_cast_2;
			o.Smoothness = _Smoothness;
			o.Alpha = _Opacity;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardSpecular alpha:fade keepalpha fullforwardshadows exclude_path:deferred vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 screenPos : TEXCOORD7;
				float4 tSpace0 : TEXCOORD1;
				float4 tSpace1 : TEXCOORD2;
				float4 tSpace2 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.screenPos = ComputeScreenPos( o.pos );
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = IN.worldPos;
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.screenPos = IN.screenPos;
				SurfaceOutputStandardSpecular o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardSpecular, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13701
1952;41;1869;1013;2238.372;1080.834;1;True;True
Node;AmplifyShaderEditor.RangedFloatNode;27;-2223.527,833.6429;Float;False;Property;_WaveSpeedMultiply;WaveSpeedMultiply;5;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.TimeNode;7;-2212.333,686.7714;Float;False;0;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-1939.025,775.7179;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;79;-1898.441,-829.3881;Float;False;Property;_FoamUVTile;FoamUVTile;10;0;1;1;20;0;1;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;76;-1586.773,-847.1892;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RegisterLocalVarNode;26;-1758.885,770.6671;Float;False;WaveSpeed;-1;True;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.GetLocalVarNode;25;-1746.885,901.6671;Float;False;26;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;34;-1025.227,130.6671;Float;False;Property;_DepthFade;DepthFade;8;0;0;0;3;0;1;FLOAT
Node;AmplifyShaderEditor.PannerNode;75;-1265.337,-842.1892;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-0.1,0.01;False;1;FLOAT;1.0;False;1;FLOAT2
Node;AmplifyShaderEditor.WorldPosInputsNode;9;-1576.397,-65.95389;Float;False;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;17;-1518.512,919.3383;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT
Node;AmplifyShaderEditor.DdxOpNode;10;-1368.397,-81.95389;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.SamplerNode;74;-1025.337,-829.1893;Float;True;Property;_FoamTexture;FoamTexture;9;0;Assets/WorldAssets/Textures/T_Foam.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;36;-1031.977,-617.9579;Float;False;Property;_FoamColour;FoamColour;1;0;0,0,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.DepthFade;32;-731.1019,133.6671;Float;False;1;0;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;29;-1327.641,719.9816;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.DdyOpNode;11;-1368.397,14.04611;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.NormalVertexDataNode;19;-707.5801,974.3782;Float;False;0;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;24;-1039.219,895.1376;Float;True;Property;_WaveGuide;WaveGuide;6;0;Assets/Plugins/AmplifyShaderEditor/Examples/Community/LowPolyWater/wave-pattern-001.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;45;-519.9769,98.41705;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;6;-1026.515,-377.8074;Float;False;Property;_BaseColour;BaseColour;0;0;0,0,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.CrossProductOpNode;12;-1240.397,-49.95389;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.RangedFloatNode;18;-751.8962,1130.189;Float;False;Property;_WavePower;WavePower;7;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;73;-677.3369,-776.1893;Float;False;2;2;0;FLOAT;0.0;False;1;COLOR;0;False;1;COLOR
Node;AmplifyShaderEditor.LerpOp;35;-351.2269,-388.8329;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleSubtractOpNode;22;-678.5767,883.1376;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.5;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;13;-1072.393,-15.70508;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;1E-09,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.RangedFloatNode;39;-199.7269,44.29205;Float;False;Constant;_Float0;Float 0;8;0;0.6;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-378.526,1057.459;Float;False;2;2;0;FLOAT3;1.0,0,0;False;1;FLOAT;0.0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;83;-396.6804,276.3872;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.ComponentMaskNode;16;-1804.191,1002.174;Float;False;False;True;False;True;1;0;FLOAT3;0,0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;-9.476929,19.54205;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.RangedFloatNode;68;311.4004,-276.5106;Float;False;Property;_Opacity;Opacity;2;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;1;310.7588,-202.668;Float;False;Property;_Spec;Spec;3;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;2;304.7588,-119.668;Float;False;Property;_Smoothness;Smoothness;4;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.PosVertexDataNode;15;-2042.955,982.9019;Float;False;0;0;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.NormalizeNode;14;-936.397,14.04611;Float;False;1;0;FLOAT3;0.0,0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.SimpleDivideOpNode;81;-604.6804,291.3872;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SurfaceDepthNode;80;-897.6804,246.3872;Float;False;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;-230.3597,989.7528;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT3;0;False;1;FLOAT3
Node;AmplifyShaderEditor.RangedFloatNode;82;-930.6804,336.3872;Float;False;Property;_TEST;TEST;11;0;0;0;10240;0;1;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;295.75,-45.375;Float;False;True;2;Float;ASEMaterialInspector;0;0;StandardSpecular;WaterShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;Back;1;0;False;0;0;Transparent;0.5;True;True;0;False;Transparent;Transparent;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;8;0;7;2
WireConnection;8;1;27;0
WireConnection;76;0;79;0
WireConnection;26;0;8;0
WireConnection;75;0;76;0
WireConnection;17;0;25;0
WireConnection;10;0;9;0
WireConnection;74;1;75;0
WireConnection;32;0;34;0
WireConnection;29;1;17;0
WireConnection;11;0;9;0
WireConnection;24;1;29;0
WireConnection;45;0;32;0
WireConnection;12;0;10;0
WireConnection;12;1;11;0
WireConnection;73;0;74;1
WireConnection;73;1;36;0
WireConnection;35;0;73;0
WireConnection;35;1;6;0
WireConnection;35;2;45;0
WireConnection;22;0;24;1
WireConnection;13;0;12;0
WireConnection;20;0;19;0
WireConnection;20;1;18;0
WireConnection;83;0;80;0
WireConnection;83;1;82;0
WireConnection;38;0;35;0
WireConnection;38;1;39;0
WireConnection;14;0;13;0
WireConnection;81;0;80;0
WireConnection;81;1;82;0
WireConnection;21;0;22;0
WireConnection;21;1;20;0
WireConnection;0;0;35;0
WireConnection;0;1;14;0
WireConnection;0;2;38;0
WireConnection;0;3;1;0
WireConnection;0;4;2;0
WireConnection;0;9;68;0
WireConnection;0;11;21;0
ASEEND*/
//CHKSM=6AE89DDE91ACF316F023991FE2A391CEA76B3695