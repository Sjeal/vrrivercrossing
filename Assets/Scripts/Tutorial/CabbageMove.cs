﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class CabbageMove : MonoBehaviour {

	public Transform endPosition;
	public AudioClip CabbageMoveFX;

	public void MoveCabbage(){

		gameObject.transform.position = endPosition.position;
		Utility.PlayAudioClip(CabbageMoveFX, gameObject, 0.5f, true);
		GetComponent<VRTK_InteractableObject> ().isUsable = false;
	}
		
}
