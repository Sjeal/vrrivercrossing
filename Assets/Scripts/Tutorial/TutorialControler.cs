﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using UnityEngine.UI;
using UnityEngine.Video;

public class TutorialControler : MonoBehaviour {

	public GameObject player;

	public VRTK_InteractableObject[] vrtkInteract;
	public GameObject CabbageObject;
	public GameObject Button;
	bool check;
	bool check2;
	public GameObject TutorialTextPart1;
	public GameObject TutorialTextPart2;
	public GameObject TutorialTextPart3;
	public GameObject controlergraphic;
	public AudioClip InteractFX;

	public GameObject VideoPlane;
	public VideoPlayer vp;

	public GameObject button2;

	void Start(){
		check2 = false;
		check = true;
		button2.SetActive (false);
		Button.SetActive (false);
		vp.Stop();
		vp.loopPointReached += CheckOver;
	}
		

	// Update is called once per frame
	void Update () {

		if (check) {
			for (int i = 0; i < vrtkInteract.Length; i++) {

				if (vrtkInteract [0].isUsable == false && vrtkInteract [1].isUsable == false && vrtkInteract [2].isUsable == false && vrtkInteract [3].isUsable == false) {
					StartPart2();
					check = false;
				}
			}
		}
	}

	void StartPart2(){
		Utility.PlayAudioClip(InteractFX, gameObject, 0.5f, true);
		TutorialTextPart1.SetActive (false);
		TutorialTextPart2.SetActive (true);
		Button.SetActive (true);

	}

	public void Part3(){
		Utility.PlayAudioClip(InteractFX, gameObject, 0.5f, true);
		TutorialTextPart2.SetActive (false);
		controlergraphic.SetActive (false);
		TutorialTextPart3.SetActive (true);
		VideoPlane.SetActive (true);
		vp.Play ();
		check2 = true;
	}
	public void Part4(){

		button2.SetActive (true);

	}

	void CheckOver(UnityEngine.Video.VideoPlayer vp)
	{
		Part4 ();
	}

	public void Part5(){

		player.transform.position = new Vector3 (0.66f, .0043f, 3.75f);

	}
}
