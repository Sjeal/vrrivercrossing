﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Static class with useful some useful functions
/// 
/// Author: Joshua Reason
/// </summary>
public static class Utility {

    /// <summary>
    /// Used to get the total bounds of all renderes in game object and it's children
    /// </summary>
    /// <param name="input">Gameobject to get bounds from</param>
    /// <returns>Calculated bound of all renderers on object</returns>
    public static Bounds calcTotalRenderBounds(GameObject input)
    {
        //Get list of all renderers in Gameobject
        Renderer[] AllRenderers = input.GetComponentsInChildren<Renderer>();

        //if non return zero-sized bound at gameobjects position
        if (AllRenderers.Length == 0)
            return new Bounds(input.transform.position,Vector3.zero);

        //Get the first bound in the renderers as a start point
        Bounds retVal = AllRenderers[0].bounds;

        //Grow bounds for each other renderer in Object
        foreach (Renderer renderer in input.GetComponentsInChildren<Renderer>())
            retVal.Encapsulate(renderer.bounds);

        //Return grown bound
        return retVal;
    }

    /// <summary>
    /// Plays an audio clip on object
    /// </summary>
    /// <param name="clip">Audio clip to play</param>
    /// <param name="destination">object to play it from</param>
    /// <param name="Volume">Volume to play it at</param>
    /// <param name="is3DSound">If true spacial blending will use spacial blending</param>
    public static void PlayAudioClip(AudioClip clip, GameObject destination, float Volume = 1, bool is3DSound = true)
    {
        //return if no sound is provided
        if (clip == null)
            return;

        //If no destination is supplied create an empty object and set the sound to be 2d;
        if (destination == null) {
            destination = new GameObject();
            is3DSound = true;
            
            //Destroy empty after use
            Object.Destroy(destination, clip.length);            
        }

        //Get AudioSource on Object
        AudioSource source = destination.GetComponent<AudioSource>();

        //If one exists copy it
        if (source != null)
        {
            source = CopyComponent(source, destination);
        }
        else // else set up a default one
        {
            source = destination.AddComponent<AudioSource>();
            source.spatialBlend = (is3DSound)? 1 : 0;
            source.playOnAwake = false;
        }

        //set volume
        source.volume = 1;

       // source.pitch = 1 + Random.Range(-0.25f, 0.25f);

        //play clip and destroy the new Source when finished
        source.clip = clip;
        source.Play();
        Object.Destroy(source, clip.length); 
    }

    /// <summary>
    /// Copies a component and applies it to gameobject
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="original"></param>
    /// <param name="destination"></param>
    /// <returns></returns>
    public static T CopyComponent<T>(T original, GameObject destination) where T : Component
    {
        System.Type type = original.GetType();
        Component copy = destination.AddComponent(type);
        System.Reflection.FieldInfo[] fields = type.GetFields();
        foreach (System.Reflection.FieldInfo field in fields)
        {
            field.SetValue(copy, field.GetValue(original));
        }
        return copy as T;
    }

}
