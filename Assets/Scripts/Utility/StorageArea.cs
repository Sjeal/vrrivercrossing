﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StorageArea
{
    public Seat[] Seats;

    public bool AddItem(PuzzleItem item)
    {

        foreach (Seat seat in Seats)
        {
            if (!seat.isOccupied && (item.FoodChainLevel == seat.LevelAccepted || seat.LevelAccepted == -1))
            {
                seat.AddItem(item);
                item.ChangeSeat(seat.SeatLocation);
                return true;
            }
        }
        return false; //no space left
    }

    public bool RemoveItem(PuzzleItem item)
    {
        foreach (Seat seat in Seats)
        {
            if (seat.item != null && seat.item.Equals(item))
            {
                seat.Clear();
                return true;
            }
        }
        return false;
    }

    public PuzzleItem[] GetItems()
    {
        List<PuzzleItem> retVal = new List<PuzzleItem>();

        foreach (Seat seat in Seats)
        {
            if (seat.isOccupied)
                retVal.Add(seat.item);
        }
        return retVal.ToArray();
    }

    public bool ContainsItem(PuzzleItem item){
        foreach (Seat seat in Seats){
            if (seat.item != null && seat.item.Equals(item))
                return true;
        }
        return false;
    }

    public bool isSafe(out PuzzleItem itemAte, out PuzzleItem itemEating)
    {
        PuzzleItem[] shoreItems = GetItems();

        foreach (PuzzleItem item in shoreItems)
        {
            if (item.isHungry(shoreItems, out itemAte))
            {
                itemEating = item;
                return false;
            }
        }

        itemAte = null;
        itemEating = null;
        return true;
    }

    public bool isSafe()
    {
        PuzzleItem itemAte = null;
        PuzzleItem itemEating = null;
        return (isSafe(out itemAte, out itemEating));
    }

}

[System.Serializable]
public class Seat
{
    public Transform SeatLocation;
    [Tooltip("What food chain level can sit in this seat. if set to -1 anythhing can sit here")]
    public int LevelAccepted = -1;

    //[HideInInspector]
    public bool isOccupied;
   // [HideInInspector]
    public PuzzleItem item;

    public void AddItem(PuzzleItem item)
    {
        Debug.Log("Adding to " + SeatLocation.name+ ": " + item.name);
        isOccupied = true;
        this.item = item;
    }

    public void Clear()
    {
        isOccupied = false;
        item = null;
    }

}
