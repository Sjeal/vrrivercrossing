﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class HandControl : MonoBehaviour {

	private float OpenValue;
	private Animator animator;
	private VRTK_ControllerEvents ce;
	public string HandPram;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
		ce = GetComponentInParent<VRTK_ControllerEvents> ();
	}


	// Update is called once per frame
	void Update () {
		OpenValue = ce.GetTriggerAxis ();
		animator.SetFloat(HandPram,OpenValue);
	}
}
