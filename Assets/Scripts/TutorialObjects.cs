﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialObjects : MonoBehaviour {

	public AudioClip GrabPop;
	bool b_sphereInteract;

	void Start(){
		
		b_sphereInteract = false;
	}

	public void OnUse(){
		b_sphereInteract = true;
		Utility.PlayAudioClip(GrabPop, null,0.5f);
		System.Collections.Hashtable hash = new System.Collections.Hashtable();
		hash.Add("amount", new Vector3(Random.Range(0.1f,1.0f), Random.Range(0.1f,1.0f), Random.Range(0.1f,1.0F)));
		hash.Add("time", 0.6f);
		iTween.PunchScale (gameObject, hash);
	}

	public void GotoMenu(){
		SceneManager.LoadScene (0);
	}
}

