﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalSounds : MonoBehaviour {

	public AudioClip[] sounds;

	void Start ()

	{
		AudioSource audio = GetComponent<AudioSource> ();
		audio.clip = sounds[Random.Range (0, sounds.Length)];
		audio.PlayOneShot(audio.clip, 0.8f);
	}

}
