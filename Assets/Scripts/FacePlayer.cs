﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacePlayer : MonoBehaviour
{

    private GameObject player;
    private Camera playerCamera;

    // Use this for initialization
    void Start()
    {

        player = GameObject.FindGameObjectWithTag("Player");
        playerCamera = player.GetComponentInChildren<Camera>();

    }

    // Update is called once per frame
    void Update()
    {
        if (playerCamera != null)
        {
            transform.LookAt(player.transform);
            transform.position = Vector3.Lerp(transform.position, playerCamera.transform.forward * 5 + Vector3.up * 2, 4 * Time.deltaTime);
        }
    }
}
