﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

 public Text timerText;
 public float time = 300;
 
 void Start ()
 {
     StartCoundownTimer();
 }
 
 void StartCoundownTimer()
 {
     if (timerText != null)
     {
         timerText.text = "Time Left: 20:00:000";
		 if(time > 0.0f){
        	 InvokeRepeating("UpdateTimer", 0.0f, 0.01667f);
		 }
		
     }
 }
 
	void UpdateTimer()
	{

			time -= Time.deltaTime;
			string minutes = Mathf.Floor(time / 60).ToString("00");
			string seconds = (time % 60).ToString("00");
			string fraction = ((time * 100) % 100).ToString("000");
			timerText.text = "Time Left: " + minutes + ":" + seconds + ":" + fraction;

			if(time < 0.01f){
				timerText.text = "Finished";
			}

	}
}
