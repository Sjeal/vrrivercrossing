﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillFarmer : MonoBehaviour {

	public RagdollFarmer rdf;

	// Use this for initialization
	void Start () {
		
	}
	
    void OnCollisionEnter(Collision collision)
    {
        // Play a sound if the colliding objects had a big impact.
        if (collision.relativeVelocity.magnitude > 5){
         	 rdf.Ragdoll(true, collision.contacts[0].point, 128.0f);
		}
    }
}
