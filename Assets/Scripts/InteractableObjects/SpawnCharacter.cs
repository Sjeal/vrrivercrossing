﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class SpawnCharacter : MonoBehaviour {

	public GameObject CharacterToSpawm;
	public GameObject ExistingChar;
	public Transform SpawnLoacation;

	public void SpawnChar(){

		Destroy(ExistingChar);
		GameObject clone = Instantiate(CharacterToSpawm, SpawnLoacation.position, SpawnLoacation.rotation) as GameObject;
		ExistingChar = clone;
	}


}
