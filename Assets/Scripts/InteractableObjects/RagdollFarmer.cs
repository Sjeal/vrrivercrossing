﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.SecondaryControllerGrabActions;
using VRTK.GrabAttachMechanics;

public class RagdollFarmer : MonoBehaviour {

	public bool isdead = false;

	// Use this for initialization
	void Start () {
		
			Rigidbody[] bodies = GetComponentsInChildren<Rigidbody>();
			foreach (Rigidbody rb in bodies)
			{
				rb.isKinematic = true;
			}
			GetComponent<Animator>().enabled = true;
	}
	
	public void Ragdoll(bool isdead, Vector3 ImpactPosition, float ImpactForce){
		GetComponent<Animator>().enabled = false;

		if(isdead){
			Rigidbody[] bodies = GetComponentsInChildren<Rigidbody>();
			foreach (Rigidbody rb in bodies)
			{
				rb.isKinematic = false;
				rb.gameObject.AddComponent<VRTK_InteractableObject>();
				rb.gameObject.AddComponent<VRTK_FixedJointGrabAttach>();
				rb.gameObject.AddComponent<VRTK_SwapControllerGrabAction>();
				rb.gameObject.GetComponent<VRTK_InteractableObject>().isGrabbable = true;
				rb.gameObject.GetComponent<VRTK_InteractableObject>().grabAttachMechanicScript = rb.gameObject.GetComponent<VRTK_FixedJointGrabAttach>();
				rb.gameObject.GetComponent<VRTK_InteractableObject>().secondaryGrabActionScript = rb.gameObject.GetComponent<VRTK_SwapControllerGrabAction>();
				rb.gameObject.GetComponent<VRTK_FixedJointGrabAttach>().precisionGrab = true;
				rb.gameObject.GetComponent<VRTK_FixedJointGrabAttach>().breakForce = 15000000.0f;
				//GetComponent<Rigidbody>().AddExplosionForce(ImpactForce, ImpactPosition, 32.0f, 3.0F, ForceMode.Force);
			}
			//GetComponent<Rigidbody>().AddExplosionForce(ImpactForce, ImpactPosition, 32.0f, 3.0F, ForceMode.Force);
		}
 	}

}
