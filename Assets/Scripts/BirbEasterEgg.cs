﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BirbEasterEgg : MonoBehaviour {

	private Transform initialLocaion;

	public Vector3 NewLocation;
	bool animating;

	void Start(){
		initialLocaion = gameObject.transform;
	}

	public void BirbEE(){
		animating = true;
	}

	void Update(){
		if (animating) {
			gameObject.transform.localPosition = Vector3.Lerp (transform.localPosition, NewLocation, Time.deltaTime/10.0f);

			if(gameObject.transform.localPosition.x >= 2450.0f){
				animating = false;
			}
		}
		if (!animating) {
			gameObject.transform.localPosition = new Vector3(-1055.0f, -445.0f, 0.0f);
		}
	}

}
