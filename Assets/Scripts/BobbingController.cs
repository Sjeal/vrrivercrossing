﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Component to make attached objects float up and down;
/// 
/// Author: Joshua Reason
/// </summary>
public class BobbingController : MonoBehaviour {

    [Tooltip ("Time in seconds to complete a bob cycle")]
    public float bobPeriod = 4;
    [Tooltip ("Distance along Y axis the position will be offset and the height of its bob")]
    public float bobAmplituted = 0.025f;

    private float lastAdjustment = 0;

    //Called every Frame
    void Update()
    {
        float heightAdjustment = Mathf.Sin((Time.time * 2* Mathf.PI) / (bobPeriod)) * bobAmplituted;
        transform.Translate((heightAdjustment - lastAdjustment) * Vector3.up);
        lastAdjustment = heightAdjustment;
    }



	
}
