﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

/// <summary>
/// Component to be added to gameobjects used as a button
/// 
/// Author: Joshua Reason
/// </summary>
public class VR_InteractableButton : VR_InteractableObject
{

    public float ScaleTime = 0.5f;
    public Transform ScalingObject;

    public Color failColor;
    public Sprite failTexture;
    public SpriteRenderer spriteRenderer;

    private Color startColor;
    private Sprite startSprite;
    private Material buttonMat;


    private bool isScaling = false;

    void Start()
    {
        if (ScalingObject == null)
            ScalingObject = transform;

        buttonMat = GetComponent<Renderer>().material;
        startColor = buttonMat.color;
        startSprite = spriteRenderer.sprite;
    }

    public void ResetButton()
    {
        if (buttonMat != null)
            buttonMat.color = startColor;
        if (startSprite != null)
            spriteRenderer.sprite = startSprite;
        ScalingObject.gameObject.SetActive(true);
    }

    public void GameOver()
    {
        if (buttonMat != null)
            buttonMat.color = failColor;
        if (startSprite != null)
            spriteRenderer.sprite = failTexture;
        ScalingObject.gameObject.SetActive(true);
    }

    public override void StartUsing(VRTK_InteractUse currentUsingObject = null)
    {
        if (BoatController.isBoatMoving || !AnalyticsControl.IsTestRunning || PuzzleManager.isGameOver)
            return;

        base.StartUsing(currentUsingObject);
        StartCoroutine(scaleObject(ScalingObject, Vector3.zero, ScaleTime));
    }

    public IEnumerator scaleObject(Transform target, Vector3 endScale, float totalTime)
    {

        Vector3 startScale = target.localScale;
        float elapsedTime = 0;

        while (elapsedTime < totalTime)
        {
            target.localScale = Vector3.Lerp(startScale, endScale, (elapsedTime / totalTime));
            yield return new WaitForEndOfFrame();
            elapsedTime += Time.deltaTime;
        }

        target.gameObject.SetActive(false);
        target.localScale = startScale;
    }

}
