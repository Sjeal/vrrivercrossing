﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorGizmo : MonoBehaviour {

	public bool drawGizmos;

	void OnDrawGizmos() {
		if (drawGizmos) {
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere (transform.position, 0.2f);
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere (transform.position, 0.02f);
			Gizmos.color = Color.green;
			Gizmos.DrawRay (transform.position, transform.forward);
			}
		}



}
