﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Component for controllings the boats animation across the river
/// 
/// Author: Joshua Reason
/// </summary>
public class BoatController : MonoBehaviour
{

    #region Public Variables
    [Tooltip("Transform of the Boat object. If left empty will use transform of attached object")]
    public Transform Boat;
    [Tooltip("Transform of the player object. If left empty will use transfrom of \"Player\" tagged object")]
    public Transform Player;
    [Tooltip ("The Button GameObject, used to hide the button while in transit")]
    public VR_InteractableButton BoatButton;

    [Header("Boat Seating")]
    [Tooltip("List of possible location for an item to be put at")]
    public StorageArea BoatStorage;


    [Header("Animation Settings")]
    [Tooltip("Position of where the boat moves to on the start shore")]
    public Transform StartTransform;
    [Tooltip("Position of where the boat moves to on the end shore")]
    public Transform EndTransform;


    [Tooltip("Time in seconds the boat takes to cross the river")]
    public float CrossingTime = 3;

    [Tooltip("If checked, boat will only be able to move with the farmer")]
    public bool isFarmerRequired = false;

    [Header("Debug")]
    public bool moveBoat;

    public static bool isBoatMoving;
    #endregion

    #region Private Variables
    private bool hasBeenWarned = false;
    private bool onStartSide = true;

    private Transform currentTransform;
    private Transform nextTransform;

    #endregion

    #region Unity Functions

    private void Awake()
    {
        //Set Boat if left empty
        if (Boat == null)
            Boat = transform;
        if (Player == null)
            Player = GameObject.FindGameObjectWithTag("Player").transform;

        //parent Seats to boat
        foreach (Seat seat in BoatStorage.Seats)
            seat.SeatLocation.parent = Boat;

        currentTransform = StartTransform;
        nextTransform = EndTransform;

    }

    private void Start()
    {
        //setBoat to start position
        Boat.position = StartTransform.position;
    }

    private void Update()
    {
        if (moveBoat)
        {
            moveBoat = false;
            StartBoatMove();
        }

    }
    #endregion Unity Functions

    #region Interaction Functions

    public bool AddToBoat(PuzzleItem item)
    {
        bool retVal = false;
        bool isNearBoat = ((onStartSide) ? PuzzleManager.Instance.startShore : PuzzleManager.Instance.endShore).ContainsItem(item);
        

        if (isNearBoat)
        {
            retVal = BoatStorage.AddItem(item);

            if (retVal){
                ((onStartSide) ? PuzzleManager.Instance.startShore : PuzzleManager.Instance.endShore).RemoveItem(item);
                item.OnAddToBoat();
            }
        }

        return retVal; //returns true if added to boat;
    }

    public bool RemoveFromBoat(PuzzleItem item)
    {
        bool retVal = false;

        if (BoatStorage.RemoveItem(item))
        {
            retVal = ((onStartSide) ? PuzzleManager.Instance.startShore : PuzzleManager.Instance.endShore).AddItem(item);
            if (retVal)
                item.OnRemoveFromBoat();
        }
        return retVal;
    }

    public void StartBoatMove()
    {
        if (isFarmerRequired && !isFarmerInBoat())
        {
            Debug.Log("Farmer needs to row the raft");
            return;
        }

        PuzzleItem itemAte;
        PuzzleItem itemEating;

        if (!PuzzleManager.Instance.isMoveValid(out itemAte, out itemEating))
        {
            if (!PuzzleManager.Instance.isWarningEnabled || hasBeenWarned)
            {
                PuzzleManager.Instance.StartGameOver(itemAte, itemEating);

                if (Player != null)
                    Player.SetParent(Boat);
                StartCoroutine(LerpPositions(Boat, currentTransform.position, nextTransform.position, CrossingTime, true));
                return;
            }
            else
            {
                Debug.Log("WARNING!!");
                hasBeenWarned = true;
            }
        }
        else
        {
            if (Player != null)
                Player.SetParent(Boat);
            StartCoroutine(LerpPositions(Boat, currentTransform.position, nextTransform.position, CrossingTime));
        }



    }

    public bool isFarmerInBoat()
    {
        foreach (PuzzleItem item in BoatStorage.GetItems())
        {
            if (item.GetType().Equals(typeof(FarmerItem)))
                return true;
        }
        return false;
    }

    public void EndBoatMove(bool newSide)
    {
        Debug.Log("End boat Move");

        onStartSide = newSide;

        currentTransform = (onStartSide) ? StartTransform : EndTransform;
        nextTransform = (onStartSide) ? EndTransform : StartTransform;

        Boat.position = currentTransform.position;

        hasBeenWarned = false;

        if (Player != null)
            Player.SetParent(null);

        foreach (PuzzleItem item in BoatStorage.GetItems())
        {
            RemoveFromBoat(item);
        }

        Boat.eulerAngles = Vector3.up * ((onStartSide) ? 180 : 0);
        BoatButton.ResetButton();

        if (PuzzleManager.Instance.isPuzzleComplete())
            PuzzleManager.Instance.PuzzleComplete();
    }

    public void ResetBoat()
    {
        EndBoatMove(true);
    }



    #endregion Interaction Functions

    #region Helper Functions

    /// <summary>
    /// Interopolates the position of target transform between to positions over time
    /// </summary>
    /// <param name="target">Transform to be moved</param>
    /// <param name="startPos">Position to move from</param>
    /// <param name="endPos">Position to move to</param>
    /// <param name="moveTime">Time in seconds the movement will take</param>
    private IEnumerator LerpPositions(Transform target, Vector3 startPos, Vector3 endPos, float moveTime, bool isGameOver = false)
    {

        Debug.Log("Moving Boat");
        float timeElapsed = 0;

        isBoatMoving = true;

        while (timeElapsed < ((isGameOver) ? (moveTime / 2) : moveTime))
        {
            target.position = Vector3.Lerp(startPos, endPos, timeElapsed / moveTime);

            yield return new WaitForSeconds(0.01f);
            timeElapsed += Time.deltaTime;
        }

        isBoatMoving = false;

        if (!isGameOver)
        {         
            EndBoatMove(!onStartSide);
        }else
        {
            BoatButton.GameOver();
        }
    }



    #endregion Helper Functions

}
