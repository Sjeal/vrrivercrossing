﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarmerItem : PuzzleItem {

    public override bool isHungry(PuzzleItem[] sameShoreItems, out PuzzleItem itemAte)
    {
        //set to null by default;
        itemAte = null;
        return false; //Farmer is never hungry
    }
}
