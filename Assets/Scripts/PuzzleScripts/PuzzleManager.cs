﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class which implements the logic of river crossing problem
/// 
/// Author: Joshua Reason
/// </summary>
public class PuzzleManager : MonoBehaviour {

    public static PuzzleManager Instance;
    public static bool isGameOver = false;

    public StorageArea startShore;
    public StorageArea endShore;

    public GameObject GameOverObject;
    public GameObject WinObject;
    public GameObject player;

    public GameObject TeleportParticlesPrefab;
    public GameObject ConfettiParticles;

    public AudioClip FailJingle;
	public AudioClip OhNo;
    public AudioClip SuccessJingle;

    public AudioClip WolfEatGoat;
    public AudioClip GoatEatCabbage;

	public GameObject Goat;
	public GameObject Wolf;
	public GameObject Cabbage;
	public ObjectShake os;

    [Tooltip("If set to true, player will be allowed to make a mistake before the game getting an game over")]
    public bool isWarningEnabled = false;

    #region Unity Functions
    private void Awake()
    {
        SingletonSetup();
    }

    #endregion


    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public bool isMoveValid(out PuzzleItem itemAte, out PuzzleItem itemEating)
    {

        //Staggared if statement so itemAte isn't overridden 
        if (!startShore.isSafe(out itemAte, out itemEating))
            return false;
        if (!endShore.isSafe(out itemAte, out itemEating))
            return false;

        return true;
    }

    public bool isPuzzleComplete()
    {
        return (endShore.GetItems().Length == endShore.Seats.Length);
    }

    public void PuzzleComplete()
    {
        Debug.Log("Yay you did it!");
        Utility.PlayAudioClip(SuccessJingle, null,0.1f);

        ConfettiParticles.SetActive(true);
        WinObject.SetActive(true);

        AnalyticsControl.Instance.CreateMoveInfo("Puzzle Complete");
        AnalyticsControl.Instance.StopRecordingData();
    }

    public void StartGameOver(PuzzleItem itemAte, PuzzleItem itemEating)
    {
        StartCoroutine(GameOver(itemAte, itemEating));      
    }


    public void ResetScene()
    {
        StartCoroutine(FadeAndReset(Color.white, 4));
    }

    

    #region Helper Functions

    /// <summary>
    /// Sets up a singleton design
    /// </summary>
    private void SingletonSetup()
    {
        if (Instance != null && this != Instance)
            Destroy(gameObject);
        else
            Instance = this;
    }

    private IEnumerator GameOver(PuzzleItem itemAte, PuzzleItem itemEating)
    {
        isGameOver = true;

        //Time it takes to cross halfway into the river
        yield return new WaitForSeconds(3);

        itemAte.BeforeIsEaten();
        itemAte.BeforeDoingEating();


        yield return StartCoroutine(itemAte.WaitToForGaze());
        Utility.PlayAudioClip(FailJingle, Camera.main.gameObject,0.005f);

        Debug.Log("ItemEaten: " + itemAte);
        itemAte.OnIsEaten();
        itemEating.OnDoingEating();

        AnalyticsControl.Instance.CreateMoveInfo("Failure: " + itemAte.itemName + " eaten"   );

        GameOverObject.transform.position = (itemAte.transform.position) + Vector3.up * 1;


        yield return new WaitForSeconds(3);
		itemAte.gameObject.SetActive (false);
        PuzzleManager.Instance.ResetScene();

    }

    private IEnumerator FadeAndReset(Color color, float TotalTime)
    {
        SteamVR_Fade.View(color, TotalTime / 2);
        yield return new WaitForSeconds(TotalTime / 2);

        foreach (PuzzleItem item in endShore.GetItems())
        {
            endShore.RemoveItem(item);
            startShore.AddItem(item);
        }

        BoatController boat = FindObjectOfType<BoatController>();
        boat.ResetBoat();

        foreach (PuzzleItem item in startShore.GetItems())
        {
            item.ResetItem();
        }

        GameOverObject.SetActive(false);
 
        SteamVR_Fade.View(Color.clear, TotalTime / 2);
        AnalyticsControl.Instance.AddAttempt();
        AnalyticsControl.Instance.CreateMoveInfo("Puzzle Reset");
        isGameOver = false;

		os.resetCabbages ();

    }


    #endregion Helper Functions

}
