﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


/// <summary>
/// Extends PuzzleItem Class to call events on state Changes
/// 
/// Author: Joshua Reason
/// </summary>
public class EventDrivenItem : PuzzleItem
{
    public UnityEvent OnIsEatenEvent = new UnityEvent();
    public UnityEvent OnDoingEatingEvent = new UnityEvent();
    public UnityEvent BeforeIsEatenEvent = new UnityEvent();
    public UnityEvent BeforeDoingEatingEvent = new UnityEvent();
    public UnityEvent OnAddToBoatEvent = new UnityEvent();
    public UnityEvent OnRemoveFromBoatEvent = new UnityEvent();
    public UnityEvent ResetItemEvent = new UnityEvent();


    public override void OnIsEaten()
    {
        base.OnIsEaten();
        OnIsEatenEvent.Invoke();
    }

    public override void OnDoingEating()
    {
        base.OnDoingEating();
        OnDoingEatingEvent.Invoke();
    }

    public override void BeforeIsEaten()
    {
        base.BeforeIsEaten();
        BeforeIsEatenEvent.Invoke();
    }

    public override void BeforeDoingEating()
    {
        base.BeforeDoingEating();
        BeforeDoingEatingEvent.Invoke();
    }

    public override void OnAddToBoat()
    {
        base.OnAddToBoat();
        OnAddToBoatEvent.Invoke();
    }

    public override void OnRemoveFromBoat()
    {
        base.OnRemoveFromBoat();
        OnRemoveFromBoatEvent.Invoke();
    }

    public override void ResetItem()
    {
        base.ResetItem();
        ResetItemEvent.Invoke();
    }
}
