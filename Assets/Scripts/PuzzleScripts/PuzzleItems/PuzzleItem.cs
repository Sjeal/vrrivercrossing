﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class which implements core functionality of Puzzle objects. 
/// 
/// Author: Joshua Reason
/// </summary>
public class PuzzleItem : MonoBehaviour
{
    [Tooltip("Will eat item one level lower, only positive levels can eat others")]
    public int FoodChainLevel;

    [Tooltip("Item name, used for Data")]
    public string itemName;

    [Tooltip ("Audioclip to play before an item is eating letting the player know to turn around")]
    public AudioClip WarningSound;
    [Tooltip("Audioclip used when puzzle item is being eaten by something else")]
    public AudioClip BeingAteSound;
    [Tooltip("Audioclip used when puzzle item is eating something else")]
    public AudioClip DoingEatenSound;

    #region Unity Fucntions

    protected virtual void Start()
    {
        //Put item is starting seat
        PuzzleManager.Instance.startShore.AddItem(this);
    }


    #endregion

    #region Interaction Functions

    /// <summary>
    /// Called by VR_Interactable object when clicked
    /// </summary>
    public void ClickedOnItem()
    {
        //Adds Item to the boat
        BoatController boat = FindObjectOfType<BoatController>();
        if (!boat.AddToBoat(this))
            boat.RemoveFromBoat(this);
    }

    #endregion Interaction Functions



    #region Puzzle Logic functions

    /// <summary>
    /// Checks if item will eat anything in supplied list
    /// </summary>
    /// <param name="sameShoreItems">Items to check. Should be anything on the sameshore</param>
    /// <param name="itemAte">out value set to item which will be eaten</param>
    /// <returns>returns true if anything is edible </returns>
    public virtual bool isHungry(PuzzleItem[] sameShoreItems, out PuzzleItem itemAte)
    {
        //set to null by default;
        itemAte = null;

        //Items of zero or less can not eat anything;
        if (this.FoodChainLevel < 1)
            return false;

        //loops through list, no need to check if item is trying to eat self since they will be the same foodchainlevel
        foreach (PuzzleItem item in sameShoreItems)
        {
            if (item.FoodChainLevel == this.FoodChainLevel - 1) //can only eat items one level lower
            {
                //if item is edible return item and true
                itemAte = item;
                return true;
            }
        }
        return false; //nothing to eat
    }

    /// <summary>
    /// Checks if item will eat anything in supplied list
    /// </summary>
    /// <param name="sameShoreItems">Items to check. Should be anything on the sameshore</param>
    /// <returns>returns true if anything is edible </returns>
    public virtual bool isHungry(PuzzleItem[] sameShoreItems)
    {
        PuzzleItem outRef;
        return isHungry(sameShoreItems, out outRef);
    }

    /// <summary>
    /// Called on Gameover on the item which is being eaten
    /// Called after player is looking at item
    /// </summary>
	public virtual void OnIsEaten()
    {

        Utility.PlayAudioClip(BeingAteSound, gameObject);

        StartCoroutine(SetInactiveAfterTime());
    }

    /// <summary>
    /// Called on Gameover on the item which is doing the eating
    /// Called after player is looking at item
    /// </summary>
    public virtual void OnDoingEating()
    {
        Utility.PlayAudioClip(DoingEatenSound, gameObject);
    }

    /// <summary>
    /// Called on Gameover on the item which is being eaten
    /// Is Called before the player has started looking at the object
    /// </summary>
    public virtual void BeforeIsEaten()
    {
        Utility.PlayAudioClip(WarningSound, Camera.main.gameObject);
    }

    /// <summary>
    /// Called on Gameover on the item which is doing the eating
    /// Is Called before the player has started looking at the object
    /// </summary>
    public virtual void BeforeDoingEating()
    {

    }

    /// <summary>
    /// Called when the item is added to the boat
    /// </summary>
    public virtual void OnAddToBoat()
    {
        //Debug.Log("Added " + name + " to boat");
    }

    /// <summary>
    /// Called when the Item is removed from the boat
    /// </summary>
    public virtual void OnRemoveFromBoat()
    {
        //Debug.Log("Added " + name + " to boat");
    }

    /// <summary>
    /// Called when the Item is reset
    /// </summary>
    public virtual void ResetItem()
    {
        gameObject.SetActive(true);
    }

    #endregion Puzzle Logic Functions


    #region Helper Functions

    /// <summary>
    /// Moves and rotates the attached gameobject to the new seat position
    /// </summary>
    /// <param name="targetSeat">Transform to move to</param>
    public virtual void ChangeSeat(Transform targetSeat)
    {
        Destroy(Instantiate(PuzzleManager.Instance.TeleportParticlesPrefab, transform.position, Quaternion.identity), 2f);

        transform.position = targetSeat.position;
        float wantedY = targetSeat.eulerAngles.y;
        transform.Rotate(Vector3.up, wantedY - transform.eulerAngles.y);
        transform.parent = targetSeat;
    }

    /// <summary>
    /// IEnumerator which waits for player to look at the PuzzleItem
    /// </summary>
    /// <returns></returns>
    public IEnumerator WaitToForGaze()
    {

        //get Camera attached to player + Camera limits
        Camera gazeCamera = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Camera>();
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(gazeCamera);

        //Get bounds of the puzzleItem renderers
        Bounds bounds = Utility.calcTotalRenderBounds(gameObject);

        //loop while the player isn't looking at the item
        while (!(GeometryUtility.TestPlanesAABB(planes, bounds)))
        {
            //recalculate planes since camera would of moved
            planes = GeometryUtility.CalculateFrustumPlanes(gazeCamera);
            yield return new WaitForEndOfFrame(); //wait for next frame to loop
        }

        //wait so this doesn't finish just as the item comes onto screen
        yield return new WaitForSeconds(0.5f);
    }

    public IEnumerator SetInactiveAfterTime(float waitTime = 4)
    {
        yield return new WaitForSeconds(waitTime);
        gameObject.SetActive(false);

    }

    #endregion Helper Functions





}
