﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Extends PuzzleItem to work implement animations on different states of the Item
/// 
/// Author: Joshua Reason
/// </summary>

[RequireComponent(typeof(Animator))]
public class AnimatedItem : PuzzleItem
{

    protected Animator anim;

    protected virtual void Awake()
    {
        anim = GetComponentInChildren<Animator>();

    }

    public override void OnIsEaten()
    {
        base.OnIsEaten();
        anim.SetTrigger("Die");
    }

    public override void OnDoingEating()
    {
        base.OnDoingEating();
        anim.SetTrigger("Eat");
    }

    public override void OnAddToBoat()
    {
        base.OnAddToBoat();
        anim.SetBool("isSitting", true);
    }

    public override void OnRemoveFromBoat()
    {
        base.OnRemoveFromBoat();
        anim.SetBool("isSitting", false);
    }

    public override void ResetItem()
    {
        base.ResetItem();
        anim.SetTrigger("Idle");
    }

}
