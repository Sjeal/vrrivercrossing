﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHelper : MonoBehaviour {

	public Animator AnimC;

	// Use this for initialization
	void Start () {
		AnimC = GetComponent<Animator> ();
	}
	
	public void PlayAnimationState(string AnimName){

		AnimC.Play (AnimName, -1, 1.0f);

	}
}
