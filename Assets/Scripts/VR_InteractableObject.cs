﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using UnityEngine.Events;

public class VR_InteractableObject : VRTK_InteractableObject  {


[Tooltip("Can touch the object while holding down the Use button")]
    public bool isUsableOnTouch = false;


[Tooltip("Time before a player can inteact with this object again")]
    public float InteractionCooldDown = 0.1f;

    public UnityEvent OnSelect = new UnityEvent();
    public Material HighLightMaterial;

    public AudioClip InteractionSound;

    private float lastUseTime = 0.0f;

    public override void StartTouching(VRTK_InteractTouch currentTouchingObject = null)
    {
        base.StartTouching(currentTouchingObject);
        AddHighLight();

        if (isUsableOnTouch){
            VRTK_InteractUse useScript = currentTouchingObject.gameObject.GetComponent<VRTK_InteractUse>();
            if (useScript != null && useScript.IsUseButtonPressed()){
                useScript.AttemptUse();
            }
        }
    }

    public override void StopTouching(VRTK_InteractTouch previousTouchingObject = null)
    {
        base.StopTouching(previousTouchingObject);
        RemoveHighLight();
    }

    public override void StartUsing(VRTK_InteractUse currentUsingObject = null)
    {
        if (BoatController.isBoatMoving || !AnalyticsControl.IsTestRunning || PuzzleManager.isGameOver)
            return;

        if (Time.time - lastUseTime < InteractionCooldDown)
            return;

        base.StartUsing(currentUsingObject);

        Utility.PlayAudioClip(InteractionSound, gameObject);

        OnSelect.Invoke();
        lastUseTime = Time.time;
    }

    private void AddHighLight()
    {
        foreach(Renderer render in GetComponentsInChildren<Renderer>())
        {
            List<Material> mats = new List<Material>(render.materials);
            mats.Add(HighLightMaterial);
            render.materials = mats.ToArray();
        }
    }

    private void RemoveHighLight()
    {
        foreach (Renderer render in GetComponentsInChildren<Renderer>())
        {
            List<Material> mats = new List<Material>(render.materials);
            mats.RemoveAll(mat => mat.Equals(HighLightMaterial));
            render.materials = mats.ToArray();
        }
    }
}
