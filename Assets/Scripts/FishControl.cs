﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishControl : MonoBehaviour {

    public Renderer rend;

    void Start() {
    	float[] OffsetValues = new float[5];
			OffsetValues [0] = 0.00f;
			OffsetValues [1] = 0.25f;
			OffsetValues [2] = 0.60f;
			OffsetValues [3] = 0.80f;
            OffsetValues [4] = 0.95f;

        rend = GetComponentInChildren<Renderer>();
        rend.material.SetFloat("_Speed", Random.Range(1f, 10f)); //Speed
        rend.material.SetFloat("_Intensity", Random.Range(0.01f, 0.02f)); //Vertex
        rend.material.SetTextureOffset("_MainTex", new Vector2(OffsetValues[Random.Range(0, 4)],OffsetValues[Random.Range(0, 4)]));
    }
       
}