﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirbControl : MonoBehaviour {

	public GameObject BirbPrefab;
	public List<Transform> BirbSpawnLocations = new List<Transform>();
	public SkinnedMeshRenderer rend;


	// Use this for initialization
	void Start () {

		float[] OffsetValues = new float[4];
		OffsetValues [0] = 0.00f;
		OffsetValues [1] = 0.25f;
		OffsetValues [2] = 0.50f;
		OffsetValues [3] = 0.75f;

		//initiate the spawn list and get all the transforms.
		List<Transform> children = new List<Transform> ();
		foreach (Transform child in this.gameObject.transform) {
			BirbSpawnLocations.Add (child.gameObject.transform);
		}
			
		//spawn a birb at a random amount of the spawn loacations
		for (int j = 0; j < BirbSpawnLocations.Count; j++) {
			int randomWeight = Random.Range (0, BirbSpawnLocations.Count);
			if(randomWeight >= BirbSpawnLocations.Count/2){
				GameObject birbClones = Instantiate (BirbPrefab, BirbSpawnLocations[j].transform.position, BirbSpawnLocations[j].rotation) as GameObject;
					float RandomSize = Random.Range(0.8f,1.4f);
					birbClones.transform.localScale = new Vector3 (RandomSize, RandomSize, RandomSize);
					birbClones.transform.parent = BirbSpawnLocations [j].transform;
				birbClones.GetComponent<Animator>().Play("BirbIdle");
				birbClones.GetComponent<Animator> ().speed = Random.Range (0.5f, 1.1f);
				//birbClones.GetComponent<Animator> ().

				rend = birbClones.GetComponentInChildren<SkinnedMeshRenderer>();
				rend.material.mainTextureOffset = new Vector2(OffsetValues[Random.Range(0,OffsetValues.Length)], OffsetValues[Random.Range(0,OffsetValues.Length)]);
			}
		}
	}
}
