﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// component to make Object Shake
/// 
/// Author: Stephen Jeal
/// </summary>
public class ObjectShake : MonoBehaviour
{

	public float shakeAmount = 0.7f;
	public List<Transform> ItemTransforms = new List<Transform>();

	public bool b_Shaking;

	public Vector3[] originalPos;
	
	void Start()
	{
		List<Transform> children = new List<Transform> ();
		foreach (Transform child in this.gameObject.transform) {
			ItemTransforms.Add (child.gameObject.transform);
		}
		originalPos = new Vector3[ItemTransforms.Count];
		for (int t = 0; t < ItemTransforms.Count; t++) {
			originalPos[t] = ItemTransforms[t].localPosition;
		}
		b_Shaking = false;
	}

	void Update(){
		if (b_Shaking) {
			float ShrinkScale;
			for (int i = 0; i < ItemTransforms.Count; i++) {
					ItemTransforms[i].localPosition = originalPos[i] + Random.insideUnitSphere * shakeAmount;
					ItemTransforms [i].localScale = Vector3.Lerp (ItemTransforms [i].localScale, new Vector3 (0.0f, 0.0f, 0.0f), Time.deltaTime / 4.0f);
			}

		}
	}

    public void startShake()
    {
        b_Shaking = true;
    }
	public void resetCabbages(){

		b_Shaking = false;
		for (int i = 0; i < ItemTransforms.Count; i++) {
			float randomSize = Random.Range(90.0f, 120.0f);
			ItemTransforms [i].localScale =  new Vector3 (randomSize, randomSize, randomSize);
		}

	}
}

