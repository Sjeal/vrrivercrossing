﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateMesh : MonoBehaviour {

	public Vector3 RotationDirection;

	public float speed;
	// Update is called once per frame
	void Update () {

		transform.Rotate(RotationDirection * speed * Time.deltaTime);

	}
}
