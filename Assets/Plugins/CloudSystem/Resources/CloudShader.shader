// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "CloudShader"
{
	Properties
	{
		_Albedo("Albedo", Color) = (0.9411765,0.9411765,0.9411765,0)
		_Spec("Spec", Range( 0 , 1)) = 0
		_Roughness("Roughness", Range( 0 , 1)) = 0
		_DisplacmentPower("DisplacmentPower", Range( 0 , 1)) = 0
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		_Size("Size", Range( 0.1 , 2)) = 0.25
		_TextureSample2("Texture Sample 2", 2D) = "white" {}
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_Emission("Emission", Range( 0 , 1)) = 0
		_Transmission("Transmission", Range( 0 , 1)) = 0
		_Opacity("Opacity", Range( 0 , 1)) = 1
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 4.6
		#pragma multi_compile_instancing
		struct Input
		{
			float2 texcoord_0;
			float2 texcoord_1;
			float2 texcoord_2;
		};

		struct SurfaceOutputStandardSpecularCustom
		{
			fixed3 Albedo;
			fixed3 Normal;
			half3 Emission;
			fixed3 Specular;
			half Smoothness;
			half Occlusion;
			fixed Alpha;
			fixed3 Transmission;
		};

		uniform float4 _Albedo;
		uniform float _Emission;
		uniform float _Spec;
		uniform float _Roughness;
		uniform float _Transmission;
		uniform float _Opacity;
		uniform sampler2D _TextureSample1;
		uniform float _Size;
		uniform sampler2D _TextureSample0;
		uniform sampler2D _TextureSample2;
		uniform float _DisplacmentPower;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float3 temp_output_89_0 = ( ase_worldPos / ( 1.0 - ( _Size * 2048.0 ) ) );
			o.texcoord_0.xy = v.texcoord.xy * (temp_output_89_0).xz + float2( 0,0 );
			float4 tex2DNode78 = tex2Dlod( _TextureSample1, float4( o.texcoord_0, 0, 0.0) );
			float4 appendResult81 = (float4(tex2DNode78.rgb , tex2DNode78.a));
			o.texcoord_1.xy = v.texcoord.xy * (temp_output_89_0).yz + float2( 0,0 );
			float4 tex2DNode75 = tex2Dlod( _TextureSample0, float4( o.texcoord_1, 0, 0.0) );
			float4 appendResult80 = (float4(tex2DNode75.rgb , tex2DNode75.a));
			float4 lerpResult73 = lerp( appendResult81 , appendResult80 , 0.0);
			o.texcoord_2.xy = v.texcoord.xy * (temp_output_89_0).xy + float2( 0,0 );
			float4 tex2DNode79 = tex2Dlod( _TextureSample2, float4( o.texcoord_2, 0, 0.0) );
			float4 appendResult82 = (float4(tex2DNode79.rgb , tex2DNode79.a));
			float4 lerpResult72 = lerp( lerpResult73 , appendResult82 , 0.0);
			float3 ase_vertexNormal = v.normal.xyz;
			v.vertex.xyz += ( ( lerpResult72 * float4( ase_vertexNormal , 0.0 ) ) * _DisplacmentPower ).xyz;
		}

		inline half4 LightingStandardSpecularCustom(SurfaceOutputStandardSpecularCustom s, half3 viewDir, UnityGI gi )
		{
			half3 transmission = max(0 , -dot(s.Normal, gi.light.dir)) * gi.light.color * s.Transmission;
			half4 d = half4(s.Albedo * transmission , 0);

			SurfaceOutputStandardSpecular r;
			r.Albedo = s.Albedo;
			r.Normal = s.Normal;
			r.Emission = s.Emission;
			r.Specular = s.Specular;
			r.Smoothness = s.Smoothness;
			r.Occlusion = s.Occlusion;
			r.Alpha = s.Alpha;
			return LightingStandardSpecular (r, viewDir, gi) + d;
		}

		inline void LightingStandardSpecularCustom_GI(SurfaceOutputStandardSpecularCustom s, UnityGIInput data, inout UnityGI gi )
		{
			UNITY_GI(gi, s, data);
		}

		void surf( Input i , inout SurfaceOutputStandardSpecularCustom o )
		{
			float4 temp_output_1_0 = _Albedo;
			o.Albedo = temp_output_1_0.rgb;
			o.Emission = ( _Albedo * _Emission ).rgb;
			float3 temp_cast_2 = (_Spec).xxx;
			o.Specular = temp_cast_2;
			o.Smoothness = ( 1.0 - _Roughness );
			float3 temp_cast_3 = (_Transmission).xxx;
			o.Transmission = temp_cast_3;
			o.Alpha = _Opacity;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardSpecularCustom keepalpha fullforwardshadows exclude_path:deferred vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.6
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = IN.worldPos;
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutputStandardSpecularCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardSpecularCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13701
1952;30;1869;1013;552.5775;127.817;1;True;True
Node;AmplifyShaderEditor.RangedFloatNode;91;-3276.666,637.1453;Float;False;Property;_Size;Size;5;0;0.25;0.1;2;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;93;-2931.345,654.4443;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;2048.0;False;1;FLOAT
Node;AmplifyShaderEditor.OneMinusNode;92;-2787.118,750.1456;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.WorldPosInputsNode;25;-3006.204,403.6873;Float;False;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleDivideOpNode;89;-2740.966,460.5455;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.ComponentMaskNode;32;-2508.844,442.3195;Float;False;True;False;True;False;1;0;FLOAT3;0,0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.ComponentMaskNode;26;-2513.039,366.39;Float;False;False;True;True;False;1;0;FLOAT3;0,0,0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.TextureCoordinatesNode;9;-2182.694,240.2318;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ComponentMaskNode;36;-2506.551,522.6899;Float;False;True;True;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT2
Node;AmplifyShaderEditor.TextureCoordinatesNode;54;-2182.017,433.9242;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;75;-1919.281,217.493;Float;True;Property;_TextureSample0;Texture Sample 0;7;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;56;-2173.809,606.767;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;78;-1908.686,410.9237;Float;True;Property;_TextureSample1;Texture Sample 1;4;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.DynamicAppendNode;80;-1579.806,246.5448;Float;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT4
Node;AmplifyShaderEditor.DynamicAppendNode;81;-1590.206,427.245;Float;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT4
Node;AmplifyShaderEditor.SamplerNode;79;-1908.686,605.9238;Float;True;Property;_TextureSample2;Texture Sample 2;6;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.DynamicAppendNode;82;-1588.905,619.6453;Float;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT4
Node;AmplifyShaderEditor.LerpOp;73;-1307.354,429.7159;Float;False;3;0;FLOAT4;0.0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0.0;False;1;FLOAT4
Node;AmplifyShaderEditor.LerpOp;72;-1046.188,542.4595;Float;False;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0.0;False;1;FLOAT4
Node;AmplifyShaderEditor.NormalVertexDataNode;5;-1032.593,693.2825;Float;False;0;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;1;-139,-125;Float;False;Property;_Albedo;Albedo;0;0;0.9411765,0.9411765,0.9411765,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;95;-144.1593,56.53979;Float;False;Property;_Emission;Emission;8;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;6;383,-58;Float;False;Property;_Roughness;Roughness;2;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;4;-765.256,654.6321;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.RangedFloatNode;16;-1032.016,848.1257;Float;False;Property;_DisplacmentPower;DisplacmentPower;3;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;94;198.7029,9.198975;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.RangedFloatNode;99;-32.6264,160.3573;Float;False;Property;_Transmission;Transmission;9;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;7;386,-133;Float;False;Property;_Spec;Spec;1;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;-599.1763,735.4616;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0.0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.OneMinusNode;8;649,-57;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;100;135.4225,270.183;Float;False;Property;_Opacity;Opacity;10;0;1;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;469.66,23.19;Float;False;True;6;Float;ASEMaterialInspector;0;0;StandardSpecular;CloudShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;True;Back;0;0;False;0;0;Translucent;0.5;True;True;0;False;Opaque;Transparent;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;1;15;10;25;False;0.5;True;0;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;93;0;91;0
WireConnection;92;0;93;0
WireConnection;89;0;25;0
WireConnection;89;1;92;0
WireConnection;32;0;89;0
WireConnection;26;0;89;0
WireConnection;9;0;26;0
WireConnection;36;0;89;0
WireConnection;54;0;32;0
WireConnection;75;1;9;0
WireConnection;56;0;36;0
WireConnection;78;1;54;0
WireConnection;80;0;75;0
WireConnection;80;3;75;4
WireConnection;81;0;78;0
WireConnection;81;3;78;4
WireConnection;79;1;56;0
WireConnection;82;0;79;0
WireConnection;82;3;79;4
WireConnection;73;0;81;0
WireConnection;73;1;80;0
WireConnection;72;0;73;0
WireConnection;72;1;82;0
WireConnection;4;0;72;0
WireConnection;4;1;5;0
WireConnection;94;0;1;0
WireConnection;94;1;95;0
WireConnection;15;0;4;0
WireConnection;15;1;16;0
WireConnection;8;0;6;0
WireConnection;0;0;1;0
WireConnection;0;2;94;0
WireConnection;0;3;7;0
WireConnection;0;4;8;0
WireConnection;0;6;99;0
WireConnection;0;9;100;0
WireConnection;0;11;15;0
ASEEND*/
//CHKSM=8D5EA687B1B43E7C4F39E40A25574CCF2561B5A6