﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class InGameDisplays : MonoBehaviour {

	public AnalyticsControl ac;

	public Text dSubjectName;
	public Text dSubjectAge;
	public Text dSubjectSex;

	public Text dNoOfAttempts;
	public Text dNoOfMoves;
	public Text dTotalTime;
	public Text dDateCode;


	// Update is called once per frame
	void Update () {

		dSubjectName.text = ac.sSubjectName;
		dSubjectAge.text = ac.sSubjectAge;
		dSubjectSex.text = ac.sSubjectSex;
		dNoOfAttempts.text = ac.NoOfAttempts.ToString();
		dNoOfMoves.text = ac.NoOfMoves.ToString();
		dTotalTime.text = ac.GameTime.ToString() + " Seconds";
		dDateCode.text = ac.CurDate.ToString() + " at " + ac.CurTime.ToString();
	}
}
