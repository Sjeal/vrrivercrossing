﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using UnityEngine.SceneManagement;
using WindowsInput;

/// <summary>
/// Implements analytic functions
/// 
/// Author: Stephen Jeal
/// </summary>
public class AnalyticsControl : MonoBehaviour {

    //-----------------------------------// Static Instace (josh added this)
    public static AnalyticsControl Instance;
    public static bool IsTestRunning = true;

    //-----------------------------------// Public Variables for the win.
    public GameObject StartUIObject;
	public GameObject PlayUIObject;

	public Text SubjectName;
	public Text SubjectAge;
	public Text SubjectSex;

	public Text ContactEmailAdress;
	public Text ContactPhoneNumber;

	public Toggle RecordVideo;
	public Toggle RecordAnalytics;

	public int NoOfAttempts = 1;
	public int NoOfMoves;
	public float TimeForMoves;
	public float TotalTime;

	private DirectoryInfo folder;
	public string CurDate;
	public string CurTime;
	private string WritingDirectory;
	private GameObject MainCam;

	public Text VideoInfotext;
	public GameObject videoinfoPanel;
	private bool InfoPanelToggle;

	//-----------------------------------// Public Variables for the win.

	//----------------------------------// Private STuff for privateness.
	

	public string sSubjectName;
	public string sSubjectAge;
	public string sSubjectSex;

	public string sContactEmailAdress;
	public string sContactPhoneNumber;

	public float GameTime;
	private float startTime = 0;
    private float lastMoveTime = 0;

    private List<MoveInfo> moveInfos = new List<MoveInfo>();

	//----------------------------------// Private STuff for privateness.
	void Start(){
		IsTestRunning = false;
		StartUIObject.SetActive (true);
		PlayUIObject.SetActive (false);

        SteamVR_Fade.View(Color.white,0);

        //set up instance (josh added this)
        Instance = this;
		InfoPanelToggle = false;
	}

	void Update (){
		//Timer
		CurDate = System.DateTime.Now.ToString("dd-MM-yyyy");
		CurTime = System.DateTime.Now.ToString("HH.mm");
		if(IsTestRunning){
			GameTime = Time.time - startTime;
		}
	}

	//TEST START
	public void StartTest(){
		//initialsation
		StartRecordScreen ();
        SteamVR_Fade.View(Color.clear, 0.5f);
        sSubjectName = SubjectName.text.ToString ();
		sSubjectAge = SubjectAge.text.ToString ();
		sSubjectSex = SubjectSex.text.ToString();
		sContactEmailAdress = ContactEmailAdress.text.ToString ();
		sContactPhoneNumber = ContactPhoneNumber.text.ToString ();
		IsTestRunning = true;
		startTime = Time.time;

		DateTime theTime = DateTime.Now;

		folder = Directory.CreateDirectory("C:/VR River Crossing Data/" + sSubjectName + " - " + CurDate + " at " + CurTime); 
		WritingDirectory = "C:/VR River Crossing Data/" + folder.ToString();
		print ("starting test");
		StartUIObject.SetActive (false);
		PlayUIObject.SetActive (true);
	}

    public void AddMove()
    {
        NoOfMoves++;
    }

    public void AddAttempt()
    {
        NoOfAttempts++;
    }



    public void StopRecordingData()
    {
        IsTestRunning = false;
    }

	public void SetupVideo(){
		InfoPanelToggle = !InfoPanelToggle;
		if (InfoPanelToggle) {
			VideoInfotext.text = "CLOSE";
			videoinfoPanel.SetActive (true);
		}
		else{
			VideoInfotext.text = "SETUP INFO";
			videoinfoPanel.SetActive (false);
		}
	}

	public void EndTest(){
		IsTestRunning = false;
		WriteDataToFile ();
		EndRecordScreen ();
		StartUIObject.SetActive (false);
        SteamVR_Fade.View(Color.white, 0.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		print("Test Has ended");
	}

	void StartRecordScreen(){
		if (RecordVideo.isOn) {
		//ShadowPlay
			//InputSimulator.SimulateKeyDown (VirtualKeyCode.MENU);
			//InputSimulator.SimulateKeyDown (VirtualKeyCode.F9);
			//InputSimulator.SimulateKeyUp (VirtualKeyCode.MENU);
			//InputSimulator.SimulateKeyUp (VirtualKeyCode.F9);
		//WindowsRecorder
			InputSimulator.SimulateKeyDown (VirtualKeyCode.LWIN);
			InputSimulator.SimulateKeyDown (VirtualKeyCode.MENU);
			InputSimulator.SimulateKeyDown (VirtualKeyCode.VK_R);
			InputSimulator.SimulateKeyUp (VirtualKeyCode.LWIN);
			InputSimulator.SimulateKeyUp (VirtualKeyCode.MENU);
			InputSimulator.SimulateKeyUp (VirtualKeyCode.VK_R);

		}
	}
	void EndRecordScreen(){
		if (RecordVideo.isOn) {
			//ShadowPlay
			//InputSimulator.SimulateKeyDown (VirtualKeyCode.MENU);
			//InputSimulator.SimulateKeyDown (VirtualKeyCode.F9);
			//InputSimulator.SimulateKeyUp (VirtualKeyCode.MENU);
			//InputSimulator.SimulateKeyUp (VirtualKeyCode.F9);
			//WindowsRecorder
			InputSimulator.SimulateKeyDown (VirtualKeyCode.LWIN);
			InputSimulator.SimulateKeyDown (VirtualKeyCode.MENU);
			InputSimulator.SimulateKeyDown (VirtualKeyCode.VK_R);
			InputSimulator.SimulateKeyUp (VirtualKeyCode.LWIN);
			InputSimulator.SimulateKeyUp (VirtualKeyCode.MENU);
			InputSimulator.SimulateKeyUp (VirtualKeyCode.VK_R);
		}
	}

    public void CreateMoveInfo(string moveName)
    {
        float moveTime = GameTime - lastMoveTime;
        lastMoveTime = GameTime;
        moveInfos.Add(new MoveInfo(moveName, moveTime));
    }

    private float getAverageMoveTime()
    {
        if (moveInfos.Count == 0)
            return -1;

        float summedTime = 0;
        foreach (MoveInfo info in moveInfos)
        {
            summedTime += info.Time;
        }

        return summedTime / moveInfos.Count;


    }

#if UNITY_STANDALONE

    void OnApplicationQuit()
    {
        if (IsTestRunning)
        {
            EndTest();
        }
    }

#endif

    void WriteDataToFile(){
		if (RecordAnalytics.isOn) {
			var fileName = WritingDirectory + "/" + sSubjectName + " Logged Data" + ".txt";

			if (File.Exists (fileName)) {
				Debug.Log (fileName + " already exists.");
				return;
			}
			//Data To Wright--------------------------------
			var sr = File.CreateText (fileName);
			sr.WriteLine ("VR RIVER CROSSING TEST SUBJECT DATA");
			sr.WriteLine ("");
			sr.WriteLine ("<<<SUBJECT PERSONAL DATA>>> ");
			sr.WriteLine ("");
			sr.WriteLine ("NAME - " + sSubjectName);
			sr.WriteLine ("AGE - " + sSubjectAge);
			sr.WriteLine ("SEX - " + sSubjectSex);
			sr.WriteLine ("");
			sr.WriteLine ("<<<SUBJECT CONTACT DATA>>> ");
			sr.WriteLine ("PHONE NUMBER - " + sContactPhoneNumber);
			sr.WriteLine ("EMAIL - " + sContactEmailAdress);
			sr.WriteLine ("");
			sr.WriteLine ("");
			sr.WriteLine ("<<<SUBJECT TEST DATA>>> ");
			sr.WriteLine ("");
			sr.WriteLine ("TOTAL TEST TIME - " + GameTime.ToString () + " Seconds");
			sr.WriteLine ("TOTAL AMOUNT OF ATTEMPTS - " + NoOfAttempts.ToString ());
			sr.WriteLine ("TOTAL NUMBER OF MOVES -  " + NoOfMoves.ToString ());
			sr.WriteLine ("Average TIME FOR MOVES - " + getAverageMoveTime().ToString ());
            sr.WriteLine("");
            sr.WriteLine("<<<Actions Break Down>>>");

            sr.WriteLine("Action | TimeTaken");
            sr.WriteLine("------------------");

            foreach(MoveInfo info in moveInfos)
            {
                sr.WriteLine(info.MoveName + ", "+ info.FormattedTime);
            }


			sr.Close ();
		}
	}
		
    private struct MoveInfo{

        public string MoveName;
        public float Time;
        public string FormattedTime;

        public MoveInfo(string MoveName, float timeInSeconds)
        {
            this.MoveName = MoveName;
            Time = timeInSeconds;

            TimeSpan t = TimeSpan.FromSeconds(timeInSeconds);
            FormattedTime = t.Minutes + ":" + t.Seconds + ":" + t.Milliseconds;
        }


    }

}
