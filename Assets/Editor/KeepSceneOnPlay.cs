﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public static class KeepSceneOnPlay
{
    static bool isSceneMode;

    static KeepSceneOnPlay()
    {

        EditorApplication.playModeStateChanged += OnPlay;
        isSceneMode = GetSetting();

    }

    [MenuItem("Tools/SwapSceneMode")]
    public static void SwitchToSceneViewPlay()
    {
        setSetting(!GetSetting());
        Debug.Log("SceneMode " + (GetSetting() ? "On" : "Off"));
    }

    private static void setSetting(bool input)
    {
        int fakeBool =  (input) ? 1 : 0;
        PlayerPrefs.SetInt("SceneViewOnPlay", fakeBool);
    }

    private static bool GetSetting()
    {
        int fakeBool = PlayerPrefs.GetInt("SceneViewOnPlay", 0);
        return (fakeBool == 1);
    }

    private static void OnPlay(UnityEditor.PlayModeStateChange state)
    {
        if (EditorApplication.isPlayingOrWillChangePlaymode && isSceneMode)
        {

            EditorWindow scene = EditorWindow.GetWindow(typeof(SceneView));
            scene.Show();
        }
    }
}



